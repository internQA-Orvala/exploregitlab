import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, color: Colors.blue);

  static const List<String> listTitle = <String>[
    "Native App",
    "Hybrid App",
  ];

  static const List<String> listDetail = <String>[
    "Flutter, Kotlin",
    "Android, iOS, Web JavaScript, Dart",
  ];

  static const List<Color> listWarna = <Color>[
    Colors.red,
    Colors.grey,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView.separated(
          separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
          padding: const EdgeInsets.all(10),
          itemCount: listTitle.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              title: Text(
                listTitle[index],
                style: optionStyle,
              ),
              leading: CircleAvatar(backgroundColor: listWarna[index]),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Detail'),
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            listTitle[index],
                            style: optionStyle,
                          ),
                          Text(listDetail[index]),
                        ],
                      ),
                      actions: <Widget>[
                        TextButton(
                          child: const Text('Close'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }
}
